#include <kinect_follow/kinect_follow_node.hpp>

KinectFollowNode::KinectFollowNode()
{

  ros::NodeHandle node_h;


  double timer_freq;

  node_h.param("goal_frame_id", goal_frame_id, std::string("/odom_combined"));
  node_h.param("torso_frame_id", torso_frame_id, std::string("/torso_1"));
  node_h.param("service_name", service_name, std::string("/set_simple_goal"));
  node_h.param("pub_freq", timer_freq, 2.0);
  node_h.param("min_distance_to_change_goal", min_distance, 1.5);

  traj_follow_client = node_h.serviceClient<trajectory_follower::SetSimpleGoal>("add_two_ints");

  pub_timer = node_h.createTimer(ros::Duration(1/timer_freq), &KinectFollowNode::timer_callback, this);

}

void KinectFollowNode::timer_callback(const ros::TimerEvent& event)
{
  tf::StampedTransform torso_transform;

  trajectory_follower::SetSimpleGoalRequest simple_goal_req;
  trajectory_follower::SetSimpleGoal simple_goal_srv;
  geometry_msgs::PoseStamped cur_goal;
  try
  {
    kinect_tf_listener.lookupTransform(goal_frame_id, torso_frame_id,
                                       ros::Time(0), torso_transform);
  }
  catch (tf::TransformException &ex)
  {
    ROS_ERROR("%s",ex.what());
    ros::Duration(1.0).sleep();
  }

  cur_goal.header.frame_id = goal_frame_id;
  cur_goal.header.stamp = ros::Time::now();
  cur_goal.pose.position.x = (double)(torso_transform.getOrigin().getX());
  cur_goal.pose.position.y = torso_transform.getOrigin().getY();
  cur_goal.pose.position.z = 0.0;
  cur_goal.pose.orientation.w = 1.0;

  if(distanceCalculate(cur_goal, prev_goal) > min_distance)
  {

    simple_goal_req.SimpleGoal = cur_goal;
    simple_goal_srv.Request = simple_goal_req;

    if (traj_follow_client.call(simple_goal_req))
    {
      ROS_INFO("Goal sent: X: %f - Y:%f",
               simple_goal_srv.Request.SimpleGoal.pose.position.x,
               simple_goal_srv.Request.SimpleGoal.pose.position.y);
      prev_goal = cur_goal;
    }
    else
    {
      ROS_ERROR("Failed to call service");
    }

  }
}

double KinectFollowNode::distanceCalculate(geometry_msgs::PoseStamped point_1, geometry_msgs::PoseStamped point_2)
{
    double x = point_1.pose.position.x - point_2.pose.position.x;
    double y = point_1.pose.position.y - point_2.pose.position.y;
    double dist;

    dist = pow(x,2)+pow(y,2);           //calculating distance by euclidean formula
    dist = sqrt(dist);                  //sqrt is function in math.h

    return dist;
}

int main(int argc, char **argv)
{

  ros::init(argc, argv, "kinect_follow");

  ROS_INFO("[KinectFollowNode] - Start node");
  KinectFollowNode* my_kinect_follow_mode; // Inicializacion

  my_kinect_follow_mode = new KinectFollowNode();

  ros::spin();
}
