#ifndef KINECTFOLLOWNODE_HPP
#define KINECTFOLLOWNODE_HPP

#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <trajectory_follower/SetSimpleGoal.h>
#include <geometry_msgs/PoseStamped.h>

class KinectFollowNode
{
  public:
    KinectFollowNode();

    void timer_callback(const ros::TimerEvent& event);

    tf::TransformListener kinect_tf_listener;

    ros::ServiceClient traj_follow_client;
    ros::Timer pub_timer;
    std::string goal_frame_id;
    std::string torso_frame_id;
    std::string service_name;


    geometry_msgs::PoseStamped prev_goal;
    double min_distance;

    double distanceCalculate(geometry_msgs::PoseStamped point_1, geometry_msgs::PoseStamped point_2);

};


#endif // KINECTFOLLOWNODE_HPP
